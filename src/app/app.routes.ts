import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from './components/home-page/home-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { PrivadoPageComponent} from './components/privado-page/privado-page.component';

import {GuardService } from "./services/guard.service";

const app_routes: Routes = [
  //{path: '/', component: HomePageComponent},
  {path: 'login', component: LoginPageComponent},
  {path: 'privado', component: PrivadoPageComponent, canActivate: [GuardService]},
  //{path: 'privado', component: PrivadoPageComponent},
  //{path: '**', component: NotFoundPageComponent}
];

export const APP_ROUTING = RouterModule.forRoot(app_routes);

