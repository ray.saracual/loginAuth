import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Vistas o Componentes @Ray_Saracual
import { HomePageComponent } from './components/home-page/home-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { PrivadoPageComponent} from './components/privado-page/privado-page.component';

//Para rutas entre la APP
import {APP_ROUTING } from "./app.routes";

import { AppComponent } from './app.component';

//Servicios 

import {AuthService } from "./services/auth.service";
import {GuardService } from "./services/guard.service";

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NavbarComponent,
    LoginPageComponent,
    NotFoundPageComponent,
    PrivadoPageComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [AuthService,GuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
