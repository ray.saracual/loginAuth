import { Component, OnInit } from '@angular/core';
import {AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  profile: any;
  //public isLogin: boolean;
 // public nombreUsuario: string;
  //public emailUsuario: string;
 // public fotoUsuario: string;
   constructor(private auth:AuthService)
   {
           auth.handleAuthentication();
      }

  ngOnInit() {

    if (this.auth.userProfile) {
      this.profile = this.auth.userProfile;
    } else {
      this.auth.getProfile((err, profile) => {
        this.profile = profile;
      });
    }
    console.log(this.profile);

  }


login(){

 this.auth.login();
}

salir(){

 this.auth.logout();
}



}
